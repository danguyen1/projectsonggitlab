# Projet Javascript

## Environnement d'intégration

Disponible à l'URL suivante : [virtualdrops.cf:40200](http://virtualdrops.cf:40200)

Intégration automatique en attente de configuration GitHub Education.

Veuillez contacter Maxime POULAIN pour les accès à Jenkins.

## Structure du projet

Le fichier ``docker-compose.yml`` permet de lancer un un certain nombre de containers docker.

* Le conteneur ``proxy`` est un proxy http. La solution utilisé est [nginx](https://www.nginx.com/)

## Lancer le projet

Pour lancer le projet vous devez copier coller le fichier ``.env.sample`` en ``.env`` et le complèter avec les bonnes valeurs. 

Pour lancer le projet vous allez devoir executer la commande ``docker-compose up --build``
 
