class InvalidSongError extends Error{
    constructor(message){
        super(message)
        this.name = "InvalidSongError"
    }
}

module.exports = InvalidSongError