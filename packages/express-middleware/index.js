const express = require('express')
const { expressjwt: jwt } = require("express-jwt")
const SongDTO = require('./models/DTOs/songDTO')
const SongService = require("./services/songService")
const StatusCodes = require("http-status-codes").StatusCodes
const morgan = require('morgan')
const fs = require('fs')
const NoDataError = require('./errors/noDataError')

const router = express.Router()
const service = new SongService()

router.use(jwt({ secret: process.env.JWT_SECRET, algorithms: ["HS256"], requestProperty: 'decoded' }))

router.use(morgan('combined', {
    stream: fs.createWriteStream('./access.log', { flags: 'a' })
}))

router.get('/', (req, res) => {
    const userUUID = req.decoded.sub
    service.get(userUUID)
        .then(songs => res.send(songs))
        .catch(error => res.status(StatusCodes.BAD_REQUEST).send(error))
})

router.post('/', (req, res) => {
    const userUUID = req.decoded.sub
    const songDTO = new SongDTO(req.body)

    service.add(userUUID, songDTO)
        .then(song => res.status(StatusCodes.CREATED).send(song))
        .catch(error => res.status(StatusCodes.UNPROCESSABLE_ENTITY).send(error))
})

router.get('/:uuid', (req, res) => {
    const userUUID = req.decoded.sub
    const songUUID = req.params.uuid

    service.getSingle(userUUID, songUUID)
        .then(song => res.status(StatusCodes.OK).send(song))
        .catch(error => res.status(StatusCodes.NOT_FOUND).send())
})

router.put('/:uuid', (req, res) => {
    const userUUID = req.decoded.sub
    const songUUID = req.params.uuid
    const songDTO = new SongDTO(req.body)

    service.update(userUUID, songUUID, songDTO)
        .then(song => res.status(StatusCodes.CREATED).send(song))
        .catch(error => res.status(StatusCodes.UNPROCESSABLE_ENTITY).send(error))
})

router.delete('/:uuid', (req, res) => {
    const userUUID = req.decoded.sub
    const songUUID = req.params.uuid

    service.delete(userUUID, songUUID)
        .then(_ => res.status(StatusCodes.NO_CONTENT).send())
        .catch(error => {
            switch (error.constructor) {
                case NoDataError:
                    res.status(StatusCodes.NOT_FOUND).send(error)
                    break;
                default:
                    res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error)
                    break;
            }
        })
})

module.exports = router