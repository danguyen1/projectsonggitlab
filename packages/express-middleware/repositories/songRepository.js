const DatabaseError = require('../errors/databaseError')
const Song = require('../models/entities/song')
const db = require('better-sqlite3')(process.env.DB_PATH, {})

db.pragma('foreign_keys = ON')

class SongRepository {
    get = async(userUUID) => {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare('SELECT * FROM SONG WHERE USER = ?')
            const all = stmt.all(userUUID)

            if (all)
                resolve(reduce(all))
            else
                reject(new DatabaseError(`Unable to get songs for user ${userUUID}.`))
        })
    }

    getSingle = async (userUUID, songUUID) => {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare('SELECT * FROM SONG WHERE USER = ? AND UUID = ?')
            const response = stmt.get(userUUID, songUUID)
    
            if (response)
                resolve(new Song(response))
            else
                reject(new DatabaseError(`Unable to get song ${songUUID} for user ${userUUID}.`))
        })
    }

    add = (song) => {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare('INSERT INTO SONG VALUES(?,?,?,?,?)')
            const response = stmt.run(song.uuid, song.name, song.duration, song.mood, song.user).lastInsertRowid
    
            if (response)
                resolve(new Song(song))
            else
                reject(new DatabaseError(`Unable to add song ${song}.`))
        })
    }

    update = (song) => {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare('UPDATE SONG SET NAME = ?, DURATION = ?, MOOD = ? WHERE UUID = ? AND USER = ?')
            const response = stmt.run(song.name, song.duration, song.mood, song.uuid, song.user)
                        
            if (response)
                resolve(new Song(song))
            else
                reject(new DatabaseError(`Unable to update song ${song}.`))
        })
    }

    delete = (userUUID, songUUID) => {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare('DELETE FROM SONG WHERE UUID = ? AND USER = ?')
            resolve(stmt.run(songUUID, userUUID))
        })
    }
}

const reduce = (all) => {
    return all.reduce((songs, song) => {
        return [...songs, new Song(song)]
    }, [])
}

module.exports = SongRepository