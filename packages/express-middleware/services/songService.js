const DatabaseError = require('../errors/databaseError')
const InvalidSongError = require('../errors/invalidSongError')
const NoDataError = require('../errors/noDataError')
const SongDTO = require('../models/DTOs/songDTO')
const Song = require('../models/entities/song')
const SongRepository = require('../repositories/songRepository')
const uuid = require('uuid').v4

class SongService {
    constructor() {
        this.songRepository = new SongRepository()
    }

    get = (userUUID) => {
        return new Promise((resolve, reject) => {
            this.songRepository.get(userUUID)
                .then(songs => resolve(reduce(songs)))
                .catch(error => reject(new NoDataError(error.message)))
        })
    }

    getSingle = (userUUID, songUUID) => {
        return new Promise((resolve, reject) => {
            this.songRepository.getSingle(userUUID, songUUID)
                .then(song => resolve(new SongDTO(song)))
                .catch(error => reject(new NoDataError(error.message)))
        })
    }

    add = (userUUID, songDTO) => {
        return new Promise((resolve, reject) => {
            const song = new Song(songDTO)
            song.uuid = uuid()
            song.user = userUUID

            if (!song.valid())
                reject(new InvalidSongError(`Song ${song} is not valid.`))

            this.songRepository.add(song)
                .then(song => resolve(new SongDTO(song)))
                .catch(error => reject(new NoDataError(error.message)))
        })
    }

    update = (userUUID, songUUID, songDTO) => {
        return new Promise((resolve, reject) => {
            const song = new Song(songDTO)
            song.uuid = songUUID
            song.user = userUUID

            if (!song.valid())
                reject(new InvalidSongError(`Song ${song} is not valid.`))

            this.songRepository.update(song)
                .then(song => resolve(new SongDTO(song)))
                .catch(error => reject(new NoDataError(error.message)))
        })
    }

    delete = (userUUID, songUUID) => {
        return new Promise((resolve, reject) => {
            this.songRepository.delete(userUUID, songUUID)
                .then(response => {
                    if (response && response.changes === 1)
                        resolve(response)
                    else
                        reject(new NoDataError(`Unable to delete song ${songUUID}`))
                })
                .catch(error => reject(new DatabaseError(error.message)))
        })
    }
}

const reduce = (all) => {
    return all.reduce((songs, song) => {
        return [...songs, new SongDTO(song)]
    }, [])
}

module.exports = SongService