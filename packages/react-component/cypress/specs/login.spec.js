import { Credentials } from "../fixtures/credentials.fixture"

describe("Login using username and password", () => {
    before(() => {
        cy.song()
    })

    it("Login", () => {
        cy.login(Credentials)
    })

    it("Logout", () => {
        cy.logout()
    })
})