import { Credentials } from "../fixtures/credentials.fixture"
import { OtherSong, Song } from "../fixtures/songs.fixture"

describe("Modifying songs", () => {
    before(() => {
        cy.song()
        cy.intercept("GET", "**/api/song/").as("GetSongs")
        cy.login(Credentials)
        cy.wait("@GetSongs")
    })

    after("Logout", () => {
        cy.clearToasters()
        cy.logout()
    })

    let numberOfSongs = 0;

    it("Saving number of songs", () => {
        cy.get(".main").find('div[class="card m-2 border-0"]').then(elements => numberOfSongs = elements.length)
    })

    it(`Adding song ${Song.name}`, () => {
        cy.get("#addButton").click()
        cy.get("#inputTitle").type(Song.name)
        cy.get("#inputMood").type(`${Song.mood}{enter}`)
        cy.get("#inputDuration").type(Song.duration)
        cy.intercept("POST", "**/api/song/").as("AddSong")
        cy.get("#saveButton").should('be.enabled').click()
        cy.wait("@AddSong")
        cy.get(".main").find('div[class="card m-2 border-0"]').should('have.length', numberOfSongs + 1)
    })

    it(`Editing song ${Song.name} to be ${OtherSong.name}`, () => {
        cy.get(".main").find('div[class="card m-2 border-0"]').eq(-1).find('a').eq(1).click()
        cy.get("#inputTitle").clear().type(OtherSong.name)
        cy.get("#inputMood").type(`${OtherSong.mood}{enter}`)
        cy.get("#inputDuration").clear().type(OtherSong.duration)
        cy.intercept("PUT", "**/api/song/*").as("UpdateSong")
        cy.get("#saveButton").should('be.enabled').click()
        cy.wait("@UpdateSong")
        cy.get(".main").find('div[class="card m-2 border-0"]').should('have.length', numberOfSongs + 1)
    })

    it(`Deleting song ${OtherSong.name}`, () => {
        cy.intercept("DELETE", "**api/song/*").as("DeleteSong")
        cy.get(".main").find('div[class="card m-2 border-0"]').eq(-1).find('a').eq(2).click()
        cy.wait("@DeleteSong")
        cy.get(".main").find('div[class="card m-2 border-0"]').should('have.length', numberOfSongs)
    })
})