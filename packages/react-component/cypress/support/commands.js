Cypress.Commands.add('song', () => {
    cy.clearAllSessionStorage()
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(Cypress.config('baseUrl'))
})

Cypress.Commands.add('login', (credentials) => {
    cy.get("#username").type(credentials.username)
    cy.get("#password").type(credentials.password)
    cy.get('button[type="submit"]').click()
})

Cypress.Commands.add('logout', () => {
    cy.get("#logout").click({force: true})
})

Cypress.Commands.add('clearToasters', () => {
    cy.get('button[class="Toastify__close-button Toastify__close-button--light"').click({multiple: true})
})