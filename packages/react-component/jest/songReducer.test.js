import { SongReducer } from '../reducers/songReducer'
import { describe, expect, it } from '@jest/globals'

describe('SongReducer', () => {
  const initialState = {
    songs: [],
    error: null,
  }

  it('should add a song to the state', () => {
    const action = {
      type: 'add',
      payload: {
        data: { title: 'Test Song', artist: 'Test Artist', uuid: '123' },
      },
    }

    const newState = SongReducer(initialState, action)
    expect(initialState.songs).toHaveLength(0)
    expect(newState.songs).toHaveLength(1)
    expect(newState.songs[0]).toEqual(action.payload.data)
    expect(newState.error).toBeNull()
  })

  it('should delete a song from the state', () => {
    const initialStateWithSongs = {
      songs: [
        { title: 'Song 1', artist: 'Artist 1', uuid: '123' },
        { title: 'Song 2', artist: 'Artist 2', uuid: '456' },
      ],
      error: null,
    }

    const action = {
      type: 'delete',
      payload: { uuid: '123' },
    }

    const newState = SongReducer(initialStateWithSongs, action)
    expect(initialStateWithSongs.songs).toHaveLength(2)
    expect(newState.songs).toHaveLength(1)
    expect(newState.songs[0]).toEqual({ title: 'Song 2', artist: 'Artist 2', uuid: '456' })
    expect(newState.error).toBeNull()
  })

  it('should update a song in the state', () => {
    const initialStateWithSongs = {
      songs: [
        { title: 'Song 1', artist: 'Artist 1', uuid: '123' },
        { title: 'Song 2', artist: 'Artist 2', uuid: '456' },
      ],
      error: null,
    }

    const action = {
      type: 'update',
      payload: { data: { title: 'New Title', artist: 'New Artist', uuid: '123' } },
    }

    const newState = SongReducer(initialStateWithSongs, action)
    expect(initialStateWithSongs.songs).toHaveLength(2)
    expect(newState.songs).toHaveLength(2)
    expect(newState.songs[0]).toEqual({ title: 'New Title', artist: 'New Artist', uuid: '123' })
    expect(newState.error).toBeNull()
  })

  it('should initialize the state with songs', () => {
    const action = {
      type: 'initialize',
      payload: {
        data: [
          { title: 'Song 1', artist: 'Artist 1', uuid: '123' },
          { title: 'Song 2', artist: 'Artist 2', uuid: '456' },
        ],
      },
    }

    const newState = SongReducer(initialState, action)
    expect(newState.songs).toHaveLength(2)
    expect(newState.songs[0]).toEqual({ title: 'Song 1', artist: 'Artist 1', uuid: '123' })
    expect(newState.songs[1]).toEqual({ title: 'Song 2', artist: 'Artist 2', uuid: '456' })
    expect(newState.error).toBeNull()
  })

  it('should handle errors', () => {
    const initializeAction = {
      type: 'initialize',
      payload: {
        data: [
          { title: 'Song 1', artist: 'Artist 1', uuid: '123' },
          { title: 'Song 2', artist: 'Artist 2', uuid: '456' },
        ],
      },
    }
    const addDuplicateAction = {
      type: 'add',
      payload: { data: { title: 'Test Song', artist: 'Test Artist', uuid: '123' } },
    }

    const initializeState = SongReducer({ songs: [], error: null }, initializeAction)
    expect(initializeState.songs).toHaveLength(2)
    expect(initializeState.error).toBeNull()

    const duplicateState = SongReducer(initializeState, addDuplicateAction)
    expect(duplicateState.songs).toHaveLength(2)
    expect(duplicateState.error).toEqual('Duplicate entry')
  })
})
