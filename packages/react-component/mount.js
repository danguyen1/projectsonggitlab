import { createRoot } from 'react-dom/client';
import {
  BrowserRouter,
  Route,
  Routes
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'

import Component from './index.js'
import { Default } from './pages/default.js';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <BrowserRouter>
    <Routes>
      <Route exact path="/home" element={<Default />} />
      <Route path={`/${SUBJECT}/*`} element={<Component />} />
    </Routes>
  </BrowserRouter>
);
