import { useEffect, Fragment } from "react"
import { useNavigate } from "react-router-dom"

export const Default = () => {
  const navigate = useNavigate()
  useEffect(() => {
    navigate(`/${SUBJECT}/`)
  })
  return (<Fragment></Fragment>)
}
