import "../style/drawing.css"

export const Drawing = () => {
  return (
    <svg className="svg-filter-hide">
      <defs>
        <filter id="filterSquiggle1">
          <feTurbulence baseFrequency="0.02" numOctaves="3" seed="10" />
          <feDisplacementMap in="SourceGraphic" scale="1.2" />
        </filter>
        <filter id="filterSquiggle2">
          <feTurbulence baseFrequency="0.05" numOctaves="3" seed="10" />
          <feDisplacementMap in="SourceGraphic" scale="1.3" />
        </filter>
        <filter id="filterSquiggle3">
          <feTurbulence baseFrequency="0.04" numOctaves="3" seed="10" />
          <feDisplacementMap in="SourceGraphic" scale="1.5" />
        </filter>
        <filter id="filterSquiggle4">
          <feTurbulence baseFrequency="0.03" numOctaves="3" seed="10" />
          <feDisplacementMap in="SourceGraphic" scale="1.7" />
        </filter>
      </defs>
    </svg>
  )
}