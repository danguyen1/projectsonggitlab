import { Link } from "react-router-dom";
import logo from '../assets/vinyl.svg';
import "../style/navbar.css"
import { logout } from "../utils/utils";

export const Navbar = ({animationState, setAnimationState}) => {
  const toggleAnimation = () => {
    setAnimationState(!animationState)
  }
  
  return (
    <nav className="navbar navbar-expand-lg navbar-light sticky-top">
      <Link className="navbar-brand" to="/song/">
        <img src={logo} width="35" height="35" className="d-inline-block align-top ms-2" alt="Logo" />
      </Link>
      <Link className="navbar-brand" to="/song/">
        <h2>Songs</h2>
      </Link>
      <div className="ms-auto">
      <button className="navbar-brand d-inline btn navbar-btn" id="logout" onClick={logout}>
          <h2>Logout</h2>
        </button>
        <button className={"navbar-brand d-inline btn navbar-btn " + (animationState ? "" : "animation-disabled")} id="animation" onClick={toggleAnimation}>
          <h2>Animation</h2>
        </button>
      </div>
    </nav>
  )
}