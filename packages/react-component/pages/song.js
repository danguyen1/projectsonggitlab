import { Link } from "react-router-dom"
import logo from "../assets/vinyl.svg"
import "../style/song.css"
import { SongApiService } from "../services/songApiService";
import { formatSeconds } from "../utils/utils";
import { EnumToast, toast } from "../services/toasterService";

export const Song = ({song, dispatch}) => {
  const deleteFunc = () => {
    SongApiService.delete(song).then(() => {
      dispatch({type: 'delete', payload: song})
      toast(EnumToast.success, `Song ${song.name} deleted successfully`)
    })
  }

  return (
    <div className="card m-2 border-0">
      <img src={logo} className="card-img-top vinyl m-auto mt-2" alt="vinyl"/>
      <div className="card-body">
        <h5 className="card-title">{song.name}</h5>
        <p className="card-text">{formatSeconds(song.duration)}</p>
        <Link to={`/song/${song.uuid}`} className="btn btn-primary w-100 mt-1">Details</Link>
        <Link to={`/song/${song.uuid}/edit`} className="btn btn-warning w-100 mt-1">Edit</Link>
        <a onClick={deleteFunc} className="btn btn-danger w-100 mt-1">Delete</a>
      </div>
    </div>
  )
}
