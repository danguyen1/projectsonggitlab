import logo from '../assets/vinyl.svg'
import { Mood } from '../models/mood'
import { Song } from '../models/song'
import { useState } from 'react'
import Select from 'react-select'
import { SongApiService } from '../services/songApiService'
import { useNavigate, useParams } from 'react-router-dom'
import { EnumToast, toast } from '../services/toasterService'


export const SongAlter = ({ dispatch, songsState }) => {
    const minimumDuration = 1
    const maximumDuration = 500
    const { uuid } = useParams()
    const navigate = useNavigate();

    const song = songsState?.songs.find(x => x.uuid === uuid)

    const [alteredSong, setAlteredSong] = useState(song ?? new Song())

    const handleNameChange = (e) => {
        setAlteredSong({ ...alteredSong, name: e.target.value })
    }
    const handleMoodChange = (e) => {
        setAlteredSong({ ...alteredSong, mood: e.value })
    }
    const handleDurationChange = (e) => {
        setAlteredSong({ ...alteredSong, duration: e.target.value })
    }

    const isButtonDisabled = () => {
        return !(alteredSong && alteredSong.name && alteredSong.mood && alteredSong.duration)
    }

    let moodOptions = []
    for (const prop in Mood) {
        moodOptions.push({ value: prop, label: prop })
    }

    const add = () => {
        SongApiService.add(alteredSong).then(addedSong => {
            dispatch({ type: "add", payload: addedSong })
            toast(EnumToast.success, `Song ${alteredSong.name} added successfully`)
            navigate("/song")
        }).catch(error => {
            toast(EnumToast.error, error)
        })
    }

    const edit = () => {
        SongApiService.update(alteredSong).then(updatedSong => {
            dispatch({ type: "update", payload: updatedSong })
            toast(EnumToast.success, `Song ${alteredSong.name} edited successfully`)
            navigate("/song")
        }).catch(error => {
            toast(EnumToast.error, error)
        })
    }

    const cancel = () => {
        navigate("/song")
    }

    return (
        <div className="text-center">
            <div className="card m-auto border-0">
                <img src={logo} className="card-img-top vinyl m-auto mt-2" alt="vinyl" />
                <div className="card-body">
                    <h5>{uuid ? "Editing" : "Adding"}</h5>
                    <h5 className="card-title">{alteredSong.name === '' ? 'Title' : alteredSong.name}</h5>
                    <div className="container-fluid">
                        <form>
                            <div className="form-group">
                                <label htmlFor="inputTitle">Title</label>
                                <input type="text" className="form-control" id="inputTitle" placeholder="Never Gonna Give You Up" onChange={handleNameChange} value={alteredSong.name} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputMood">Mood</label>
                                <Select id="inputMood" options={moodOptions} onChange={handleMoodChange} value={{ value: alteredSong.mood, label: alteredSong.mood }}></Select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="inputDuration">Duration</label>
                                <input type="number" min={minimumDuration} max={maximumDuration} onChange={handleDurationChange} value={alteredSong.duration} className="form-control" id="inputDuration" placeholder="185" />
                            </div>
                        </form>
                    </div>
                    <button disabled={isButtonDisabled()} onClick={uuid ? edit : add} className="btn btn-success w-100 mt-4" id="saveButton">
                        {uuid ? "Edit" : "Add"}
                    </button>
                    <button onClick={cancel} className="btn btn-danger w-100 mt-2" id="cancelButton">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    )
}
