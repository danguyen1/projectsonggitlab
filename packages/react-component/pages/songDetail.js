import { useParams } from 'react-router-dom';
import logo from '../assets/vinyl.svg'
import { formatSeconds } from '../utils/utils';

export const SongDetail = ({ songsState }) => {
  const { uuid } = useParams()
  const songs = songsState.songs
  const song = songs.find(song => song.uuid === uuid)

  return (
    <div className="d-flex flex-row">
      <div>
        <img alt="logo" src={logo} className="m-5 w-75"></img>
      </div>
      <div className="d-flex flex-column justify-content-center">
        <h1>{song?.name ?? ""}</h1>
        <h2 className="mt-2">{formatSeconds(song?.duration ?? 0)}</h2>
        <h2 className="mt-2">{song?.mood ?? ""}</h2>
      </div>
    </div>
  )
}
