export const SongReducer = (initialState, action) => {
  const actionsMap = {
    'add': addSong,
    'delete': deleteSong,
    'initialize': initializeState,
    'update': updateSong,
  }

  const result = actionsMap[action.type]
  return result ? result(initialState, action) : initialState
}

const addSong = (state, action) => {
  if (state.songs.filter(x => x.uuid === action.payload.data['uuid']).length > 0) {
    return updateErrorState(state, new Error('Duplicate entry'))
  }

  return {
    ...state,
    songs: [...state.songs, action.payload.data],
    error: null,
  }
}

const deleteSong = (state, action) => {
  const songs = state.songs.filter(song => song.uuid !== action.payload.uuid)
  if (state.songs.length === songs.length) {
    return updateErrorState(state, new Error('The song does not exist'))
  }

  return {
    ...state,
    songs,
    error: null,
  }
}

const updateSong = (state, action) => {
  if (!state.songs.filter(song => song.uuid === action.payload.data.uuid).length) {
    return updateErrorState(state, new Error('The song does not exist'))
  }

  const songs = state.songs.map((song) =>
    song.uuid === action.payload.data.uuid ? action.payload.data : song
  )

  return {
    ...state,
    songs,
    error: null,
  }
}

const initializeState = (state, action) => {
  return {
    ...state,
    songs: action.payload.data,
    error: null,
  }
}

const updateErrorState = (state, error) => {
  return {
    ...state,
    error: error.message,
  }
}
