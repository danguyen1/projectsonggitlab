import axios from 'axios';
import { StatusCodes } from 'http-status-codes';
import { logout } from '../utils/utils';
import { baseUrlApi } from '../environment/environment';

const http = axios.create({
    baseURL: baseUrlApi,
    timeout: 1000,
    headers: {'Authorization': `Bearer ${localStorage.getItem('jwt')}` }
  });

http.interceptors.response.use((res) => res, (error) => {
    if(error.response.status === StatusCodes.UNAUTHORIZED){
        logout()
    }

    if(error.response.status >= StatusCodes.BAD_REQUEST && error.response.status < StatusCodes.INTERNAL_SERVER_ERROR)
        return Promise.reject(error)

    return error;
})

export const SongApiService = {
    get: async() => http.get('/'),

    add: async(song) => http.post('/', {name: song.name, duration: +song.duration, mood: song.mood}),
    
    update: async(song) => http.put(`/${song.uuid}`, {name: song.name, duration: +song.duration, mood: song.mood}),
    
    delete: async(song) => http.delete(`/${song.uuid}`)
}