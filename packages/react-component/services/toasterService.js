import { toast as reactToast } from "react-toastify";

export const EnumToast = {
    success: 'success',
    error: 'error'
};

export const toast = (enumToast, message) => {
    switch (enumToast) {
        case EnumToast.success:
            reactToast.success(message, {autoClose: 5000})
            break
        case EnumToast.error:
            reactToast.error(`An error occured\n${message}`)
            break
    }
}