export const logout = () => {
    localStorage.removeItem('jwt')
    window.location.href = '/'
}

export const formatSeconds = (duration) => {
    return `${Math.floor(duration / 60)}m ${duration % 60}s`;
}